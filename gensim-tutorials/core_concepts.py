#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 15:22:24 2021

@author: rachelkirby
"""

# CORE CONCEPTS ((TUTORIAL 1))

from gensim import corpora;
from gensim import models;
from gensim import similarities;
from collections import defaultdict
import pprint;

text_corpus = [
    "Human machine interface for lab abc computer applications",
    "A survey of user opinion of computer system response time",
    "The EPS user interface management system",
    "System and human system engineering testing of EPS",
    "Relation of user perceived response time to error measurement",
    "The generation of random binary unordered trees",
    "The intersection graph of paths in trees",
    "Graph minors IV Widths of trees and well quasi ordering",
    "Graph minors A survey",
]

# Create a set of frequent words
stoplist = set('for a of the and to in'.split(' '))
# Lowercase each document, split it by white space and filter out stopwords
texts = [[word for word in document.lower().split() if word not in stoplist]
         for document in text_corpus]

# Count word frequencies
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1

# Only keep words that appear more than once
processed_corpus = [[token for token in text if frequency[token] > 1] for text in texts]
pprint.pprint(processed_corpus)

# assign id to every word: dictionary sweeps through left over texts, collecting words and word counts
dictionary = corpora.Dictionary(processed_corpus)
pprint.pprint(dictionary.token2id)

# get bag of words vector representation for training corpus
# doc2bow counts occurences of each word, find id of word in dictionary and responds with vector
bow_corpus = [dictionary.doc2bow(text) for text in processed_corpus]
print('BAG OF WORDS')
pprint.pprint(bow_corpus)

# train the tf-idf model on the corpus
tfidf = models.TfidfModel(bow_corpus)

# run tf-idf model on new string
words = "system minors".lower().split()
print(tfidf[dictionary.doc2bow(words)])

# build index for documents in corpus so can query for similarity
index = similarities.SparseMatrixSimilarity(tfidf[bow_corpus], num_features=12)

# query a text across index
query_document = 'system engineering'.split()
query_bow = dictionary.doc2bow(query_document)
sims = index[tfidf[query_bow]]
print(list(enumerate(sims)))

# make it more readable                    
for document_number, score in sorted(enumerate(sims), key=lambda x: x[1], reverse=True):
    print(document_number, score)
    
