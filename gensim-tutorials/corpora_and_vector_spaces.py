#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  2 10:07:55 2021

@author: rachelkirby
"""

from pprint import pprint  # pretty-printer
from collections import defaultdict
from gensim import corpora
from smart_open import open  # for transparently opening remote files

# CORPUS STREAMING
# Long lists of documents is too much to store in RAM as above, instead can process one document at a time

# collect statistics about all tokens in remote file
dictionary = corpora.Dictionary(line.lower().split() for line in open('https://radimrehurek.com/mycorpus.txt'))
# identify stop words
stoplist = set('for a of the and to in'.split())
stop_ids = [
    dictionary.token2id[stopword]
    for stopword in stoplist
    if stopword in dictionary.token2id
]
# identify words that appear only once
once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
# remove stop words and words that only appear once
dictionary.filter_tokens(stop_ids + once_ids)  
# remove gaps in id sequence after words that were removed
dictionary.compactify()  
print(dictionary)

class MyCorpus:
    def __iter__(self):
        for line in open('https://radimrehurek.com/mycorpus.txt'):
            # assume there's one document per line, tokens separated by whitespace (this can be edited)
            yield dictionary.doc2bow(line.lower().split())
            
corpus_memory_friendly = MyCorpus()  # doesn't load the corpus into memory!
print(corpus_memory_friendly) #the corpus is now an object

for vector in corpus_memory_friendly:  # load one vector into memory at a time
    print(vector)
    
    
#CORPUS FORMATS
#There exist several file formats for serializing a Vector Space corpus

corpus = [[(1, 0.5)], []]

#save corpus into different storage formats:
corpora.SvmLightCorpus.serialize('/tmp/corpus.svmlight', corpus)
corpora.BleiCorpus.serialize('/tmp/corpus.lda-c', corpus)
corpora.LowCorpus.serialize('/tmp/corpus.low', corpus)
corpora.MmCorpus.serialize('/tmp/corpus.mm', corpus)

print(list(corpus))  #simple method of print
for doc in corpus: #more memory friendly method of print
    print(doc)

#COMPATIBILITY WITH NUMPY AND SCIPY
#to help converting from/to numpy or scipy matrices

import gensim
import numpy as np
numpy_matrix = np.random.randint(10, size=[5, 2])  # random matrix as an example
corpus = gensim.matutils.Dense2Corpus(numpy_matrix)
# numpy_matrix = gensim.matutils.corpus2dense(corpus, num_terms=number_of_corpus_features)

import scipy.sparse
scipy_sparse_matrix = scipy.sparse.random(5, 2)  # random sparse matrix as example
corpus = gensim.matutils.Sparse2Corpus(scipy_sparse_matrix)
scipy_csc_matrix = gensim.matutils.corpus2csc(corpus)