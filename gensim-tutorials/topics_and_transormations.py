#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun  2 12:01:20 2021

@author: rachelkirby
"""

import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)

from gensim import corpora
from smart_open import open  # for transparently opening remote files
from gensim import models
import os
import tempfile

dictionary = corpora.Dictionary(line.lower().split() for line in open('https://radimrehurek.com/mycorpus.txt'))
stoplist = set('for a of the and to in'.split())
stop_ids = [
    dictionary.token2id[stopword]
    for stopword in stoplist
    if stopword in dictionary.token2id
]
once_ids = [tokenid for tokenid, docfreq in dictionary.dfs.items() if docfreq == 1]
dictionary.filter_tokens(stop_ids + once_ids)  
dictionary.compactify()  
print(dictionary)

class MyCorpus:
    def __iter__(self):
        for line in open('https://radimrehurek.com/mycorpus.txt'):
            yield dictionary.doc2bow(line.lower().split())
            
corpus = MyCorpus() 
 
#CREATING A TRANSORMATION

tfidf = models.TfidfModel(corpus)  # step 1 -- initialize a model
doc_bow = [(0, 1), (1, 1)] 
print(tfidf[doc_bow]) #step 2 - tfidf is treated as a read-only object used to convert new documentation

corpus_tfidf = tfidf[corpus] #... of apply to a whole corpus
for doc in corpus_tfidf:
    print(doc)
    
# transformations can be serialised in a chain
# lsi (latent semantic indexing) transforms the corpus to a group of topics, created from the word associations
lsi_model = models.LsiModel(corpus_tfidf, id2word=dictionary, num_topics=2)  # initialize an LSI transformation
corpus_lsi = lsi_model[corpus_tfidf]  # create a double wrapper over the original corpus: bow->tfidf->fold-in-lsi
lsi_model.print_topics(2)

documents = [] 
for line in open('https://radimrehurek.com/mycorpus.txt'):
    documents.append(line);

# both bow->tfidf and tfidf->lsi transformations are actually executed here, on the fly
for doc, as_text in zip(corpus_lsi, documents):
    print(doc, as_text)



# Model persistency is achieved with the save() and load() functions:
with tempfile.NamedTemporaryFile(prefix='model-', suffix='.lsi', delete=False) as tmp:
    lsi_model.save(tmp.name)  # same for tfidf, lda, ...

loaded_lsi_model = models.LsiModel.load(tmp.name)

os.unlink(tmp.name)