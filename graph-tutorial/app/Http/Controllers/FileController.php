<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Microsoft\Graph\Model\DriveItem;
use App\TokenStore\TokenCache;
use App\TimeZones\TimeZones;


class FileController extends Controller
{
    private function getGraph(): Graph
    {
      // Get the access token from the cache
      $tokenCache = new TokenCache();
      $accessToken = $tokenCache->getAccessToken();

      // Create a Graph client
      $graph = new Graph();
      $graph->setAccessToken($accessToken);
      return $graph;
    }

    public function getDriveItems()
    {
      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();

      $driveItems = $graph->createRequest('GET', '/me/drive/root/children')
        ->setReturnType(DriveItem::class)
        ->execute();

      $sharedItems = $graph->createRequest('GET', '/me/drive/sharedWithMe')
      ->setReturnType(DriveItem::class)
      ->execute();

      $viewData['driveItems'] = $driveItems;
      $viewData['sharedItems'] = $sharedItems;

      return view('drives', $viewData);
    }

    public function getMyDriveItemsChildren( $itemId )
    {
      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();

      $graphURL = '/me/drive/items/' . $itemId . '/children';

      $children = $graph->createRequest('GET', $graphURL)
        ->setReturnType(DriveItem::class)
        ->execute();

      $viewData['driveItems'] = $children;
      return view('drives', $viewData);
    }

    public function getSharedDriveItemsChildren( $driveId, $driveItemId )
    {
      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();

      $graphURL = '/drives/' . $driveId . '/items/' . $driveItemId . '/children';

      $children = $graph->createRequest('GET', $graphURL)
        ->setReturnType(DriveItem::class)
        ->execute();
        
      $viewData['sharedItemsChildren'] = $children;
      $viewData['sharedItems'] = null;
      $viewData['driveItems'] = null;
      
      //dd($children);
      return view('drives', $viewData);
    }

    public function getShareForm( $itemId )
    {
        $viewData = $this->loadViewData();
        return view('share', compact('itemId'), $viewData);
    }

    public function shareDriveItem( Request $request )
    {
      // Validate required fields
      $request->validate([
        'itemId'  => 'required',
        'recipient' => 'required|email',
        'access' => 'required'
      ]);

      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();

      $params = [
        'requireSignIn' => false,
        'sendInvitation' => true,
        'roles' => [ $request->access ],
        'recipients' => [
            [ 'email' => $request->recipient ]
        ],
        'message' => 'Collaborate with me'
      ];

      $graphURL = '/me/drive/items/' . $request->itemId . '/invite';

      $response = $graph->createRequest('POST', $graphURL)
        ->attachBody($params)
        ->execute();

      return redirect()->route('getDriveItems', $viewData);
    }

    public function getUsersDrive( $userId )
    {
      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();

      $graphURL = '/users/' . $userId . '/drives';
      $driveItems = $graph->createRequest('GET', $graphURL)
        ->setReturnType(DriveItem::class)
        ->execute();

      dd($driveItems);
      $viewData['driveItems'] = $driveItems;
      return view('drives', $viewData);
    }
}
