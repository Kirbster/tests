<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Microsoft\Graph\Graph;
use App\TokenStore\TokenCache;
use Microsoft\Graph\Model\Site;


class SiteController extends Controller
{
    private function getGraph(): Graph
    {
      // Get the access token from the cache
      $tokenCache = new TokenCache();
      $accessToken = $tokenCache->getAccessToken();

      // Create a Graph client
      $graph = new Graph();
      $graph->setAccessToken($accessToken);
      return $graph;
    }

    public function getAllSites()
    {
        $viewData = $this->loadViewData();

        $graph = $this->getGraph();

        $sites = $graph->createRequest('GET', '/me/followedSites')
          ->setReturnType(Site::class)
          ->execute();

        // dd($sites);
        $viewData['sites'] = $sites;
        return view('sites', $viewData);
    }

    public function getSite( $siteid )
    {
        $viewData = $this->loadViewData();

        $graph = $this->getGraph();
        $url = '/sites/' . $siteid;

        $site = $graph->createRequest('GET', $url)
          ->setReturnType(Site::class)
          ->execute();

        // dd($site);     
        $viewData['site'] = $site;
        return view('site', $viewData);
    }
}
