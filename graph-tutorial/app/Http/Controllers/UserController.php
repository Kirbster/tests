<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use Microsoft\Graph\Model\User;
use Microsoft\Graph\Model\Permission;
use App\TokenStore\TokenCache;
use App\TimeZones\TimeZones;

class UserController extends Controller
{
    private function getGraph(): Graph
    {
      // Get the access token from the cache
      $tokenCache = new TokenCache();
      $accessToken = $tokenCache->getAccessToken();

      // Create a Graph client
      $graph = new Graph();
      $graph->setAccessToken($accessToken);
      return $graph;
    }

    public function getUserList()
    {
      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();

      $users = $graph->createRequest('GET', '/users')
        ->setReturnType(User::class)
        ->execute();

      $viewData['users'] = $users;
      return view('users', $viewData);
    }

    public function getUser( $userId )
    {
      $viewData = $this->loadViewData();
      
      $graph = $this->getGraph();
      
      $url = '/users/' . $userId ;

      $user = $graph->createRequest('GET', $url )
        ->setReturnType(User::class)
        ->execute();

      // dd($user);
      $viewData['thisUser'] = $user;
      return view('user', $viewData);
    }
}
