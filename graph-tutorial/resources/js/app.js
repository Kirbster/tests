require('./bootstrap');

import Vue from 'vue';

import FilePicker from "./components/FilePicker";
Vue.component('file-picker', FilePicker);

/** OneDrive File Picker */
const onedriveApp = document.getElementById('onedrive-app');

new Vue({
}).$mount(onedriveApp)