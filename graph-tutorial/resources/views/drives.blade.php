@extends('layout')

@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Created At</th>
      <th scope="col">Last Modified</th>
      <th scope="col">Size</th>
      <th scope="col">URL</th>
      <th scope="col">Share</th>
      <th scope="col">Owner</th>
    </tr>
  </thead>
  <tbody>

    <!-- driveItems -->
    @isset($driveItems)
    @foreach($driveItems as $item)
    <tr>
      <td>
        @if( ($item->getFolder() != null)  )
        <a href="{{ route('getMyDriveChildren', $item->getId() ) }}">{{ $item->getName() }}</a>
        @else
        {{ $item->getName() }}
        @endif
      </td>
      <td>{{ \Carbon\Carbon::parse($item->getCreatedDateTime())->format('n/j/y g:i A') }}</td>
      <td>{{ \Carbon\Carbon::parse($item->getLastModifiedDateTime())->format('n/j/y g:i A') }}</td>
      <td>{{ $item->getSize() }}</td>
      <td><a href="{{ $item->getWebUrl() }}">{{ $item->getWebUrl() }}</a></td>
      <td><a href="{{ route('getShareForm', $item->getId()) }}">Share</a></td>
      <td>You</td>
    </tr>
    @endforeach
    @endif

    <!-- sharedItems -->
    @isset($sharedItems)
    @foreach($sharedItems as $item)
    <tr>
      <td>
        @if( $item->getRemoteItem()->getFolder() != null )
        <a href="{{ route('getSharedDriveChildren', [ $item->getRemoteItem()->getParentReference()->getDriveId(), $item->getRemoteItem()->getId() ] ) }}">{{ $item->getName() }}</a>
        @else
        {{ $item->getName() }}
        @endif
      </td>
      <td>{{ \Carbon\Carbon::parse($item->getCreatedDateTime())->format('n/j/y g:i A') }}</td>
      <td>{{ \Carbon\Carbon::parse($item->getLastModifiedDateTime())->format('n/j/y g:i A') }}</td>
      <td>{{ $item->getSize() }} B</td>
      <td><a href="{{ $item->getWebUrl() }}">{{ $item->getWebUrl() }}</a></td>
      <td>-</td>
      <td>{{ $item->getRemoteItem()->getcreatedBy()->getUser()->getDisplayName() }} </td>
    </tr>
    @endforeach
    @endif

    <!-- sharedItemsChildren -->
    @isset($sharedItemsChildren)
    @foreach($sharedItemsChildren as $item)
    <tr>
      <td>
        @if( $item->getFolder() != null )
        <a href="{{ route('getSharedDriveChildren', [ $item->getParentReference()->getDriveId(), $item->getId() ] ) }}">{{ $item->getName() }}</a>
        @else
        {{ $item->getName() }}
        @endif
      </td>
      <td>{{ \Carbon\Carbon::parse($item->getCreatedDateTime())->format('n/j/y g:i A') }}</td>
      <td>{{ \Carbon\Carbon::parse($item->getLastModifiedDateTime())->format('n/j/y g:i A') }}</td>
      <td>{{ $item->getSize() }}</td>
      <td><a href="{{ $item->getWebUrl() }}">{{ $item->getWebUrl() }}</a></td>
      <td><a href="{{ route('getShareForm', $item->getId()) }}">Share</a></td>
      <td>You</td>
    </tr>
    @endforeach
    @endif
  </tbody>
</table>
@endsection