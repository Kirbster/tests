@extends('layout')

@section('content')

<h1>Share</h1>
<form method="post" action="{{ route('shareDriveItem') }}">
  @csrf
  <div class="form-group">
    <input type="hidden" class="form-control" id="itemId" name="itemId" value="{{ $itemId }}">
  </div>

  <div class="form-group">
    <label for="recipient">Email address</label>
    <input type="email" class="form-control @error('recipient') is-invalid @enderror" id="recipient" name="recipient" placeholder="name@example.com">
    @error('recipient')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>

  <div class="form-group">
    <label for="access">Access</label>
    <select class="form-control @error('access') is-invalid @enderror" id="access" name="access">
      <option>read</option>
      <option>write</option>
    </select>
    @error('access')
      <span class="invalid-feedback" role="alert">
        <strong>{{ $message }}</strong>
      </span>
    @enderror
  </div>

  <input type="submit" class="btn btn-primary" value="Share"/>
  
</form>
@endsection