@extends('layout')

@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">Site</th>
      <th scope="col">Description</th>
      <th scope="col">Created at</th>
      <th scope="col">URL</th>
    </tr>
  </thead>
  <tbody>
  
  @isset($site)
    <tr>
      <td>{{ $site->getDisplayName() }}</a></td>
      <td>{{ $site->getDescription() }}</td>
      <td>{{ \Carbon\Carbon::parse($site->getCreatedDateTime())->format('n/j/y g:i A') }}</td>
      <td><a href="{{ $site->getWebUrl() }}">{{ $site->getWebUrl() }}</a></td>
    </tr>
  @endif
  </tbody>
</table>

@endsection