@extends('layout')

@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">Site</th>
      <th scope="col">Collection</th>
      <th scope="col">URL</th>
    </tr>
  </thead>
  <tbody>
  @isset($sites)
    @foreach($sites as $site)
    <tr>
      <td><a href="{{ route('getSite', $site->getId()) }}">{{ $site->getDisplayName() }}</a></td>
      <td>{{ $site->getSiteCollection()->getHostname() }}</td>
      <td><a href="{{ $site->getWebUrl() }}">{{ $site->getWebUrl() }}</a></td>
    </tr>
    @endforeach
  @endif
  </tbody>
</table>
@endsection