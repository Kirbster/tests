@extends('layout')

@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Job</th>
      <th scope="col">Email</th>
      <th scope="col">Phone Number</th>
      <th scope="col">Office Location</th>
    </tr>
  </thead>
  <tbody>
        <tr>
            <td>{{ $thisUser->getDisplayName() }}</td>

            @if( $thisUser->getJobTitle() != null )
              <td>{{ $thisUser->getJobTitle() }}</td>
              @else
              <td> - </td>
            @endif

            @if( $thisUser->getMail() != null )
            <td>{{ $thisUser->getMail() }}</td>
              @else
              <td> - </td>
            @endif

            @if( $thisUser->getBusinessPhones() != null )
              <td>{{ $thisUser->getBusinessPhones()[0] }}</td>
            @else
              <td> - </td>
            @endif

            @if( $thisUser->getOfficeLocation()  != null )
              <td>{{ $thisUser->getOfficeLocation()  }}</td>
            @else
              <td> - </td>
            @endif
        </tr>
  </tbody>
</table>
@endsection
