@extends('layout')

@section('content')

<table class="table">
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Job</th>
      <th scope="col">Email</th>
      <th scope="col">Drive</th>
    </tr>
  </thead>
  <tbody>
    @isset($users)
        @foreach($users as $user)
        <tr>
            <td><a href="{{ route('getUser', $user->getId()) }}">{{ $user->getDisplayName() }}</td>
            <td>{{ $user->getJobTitle() }}</td>
            <td>{{ $user->getMail() }}</td>
            <td><a href="{{ route('getUsersDrive', $user->getId()) }}">View Drive</a>
        </tr>
        @endforeach
    @endif
  </tbody>
</table>
@endsection
