<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\CalendarController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\SiteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth
Route::get('/', [HomeController::class, 'welcome']);
Route::get('/signin', [AuthController::class, 'signin']);
Route::get('/callback', [AuthController::class, 'callback']);
Route::get('/signout', [AuthController::class, 'signout']);

// Calendar
Route::get('/calendar', [CalendarController::class, 'calendar']);
Route::get('/calendar/new', [CalendarController::class, 'getNewEventForm']);
Route::post('/calendar/new', [CalendarController::class, 'createNewEvent']);

// Drive
Route::get('/drives', [FileController::class, 'getDriveItems'])->name('getDriveItems');;
Route::get('/drive/{driveItem}/items', [FileController::class, 'getMyDriveItemsChildren'])->name('getMyDriveChildren');
Route::get('/drive/{driveId}/driveItem/{driveItemId}/items', [FileController::class, 'getSharedDriveItemsChildren'])->name('getSharedDriveChildren');
Route::get('/driveItem/{driveItem}/share/form', [FileController::class, 'getShareForm'])->name('getShareForm');
Route::post('/driveItem/share', [FileController::class, 'shareDriveItem'])->name('shareDriveItem');

// Users
Route::get('/users', [UserController::class, 'getUserList'])->name('getUserList');
Route::get('/user/{userId}', [UserController::class, 'getUser'])->name('getUser');
Route::get('/user/{userId}/drive', [FileController::class, 'getUsersDrive'])->name('getUsersDrive');

Route::get('/sites', [SiteController::class, 'getAllSites'])->name('getAllSites');
Route::get('/site/{site}', [SiteController::class, 'getSite'])->name('getSite');
