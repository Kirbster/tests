<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Everyman\Neo4j\Client;
use Everyman\Neo4j\Transport\Curl;
// require_once './vendor/autoload.php';

class everymanController extends Controller
{
    public function test(){  
        $client = new Client((new Curl('localhost',7687))
            ->setAuth('neo4j','password'));
        print_r($client->getServerInfo());
    }
}
