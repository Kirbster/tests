<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\RequestOptions;

class httpController extends Controller
{
    public function test(){
        $data = [
            "statements" => [
                  ["statement" => 'MATCH (topic:Topic {topicId: $id}) RETURN topic'],
                  ["props" => [ "id" => "1" ]]
            ]
        ];

        $response = Http::withHeaders([
            'username' => 'neo4j',
            'password' => 'password',
            'Content-Type' => 'application/json',
            'Accept' => 'application/json;charset=UTF-8'
        ])->post('http://localhost:7474/db/neo4j/tx', [ json_encode($data)]);
        // ])->get('http://localhost:7474/db/data');

        dd($response);
    }
}
