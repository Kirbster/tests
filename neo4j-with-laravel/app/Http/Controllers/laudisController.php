<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laudis\Neo4j\ClientBuilder;
use Laudis\Neo4j\Databags\Statement;

class laudisController extends Controller
{
    public function test(){
        $client = ClientBuilder::create()
            ->addHttpConnection('backup', 'http://neo4j:password@localhost:7474')
            ->addBoltConnection('default', 'bolt://neo4j:password@localhost:7687')
            ->setDefaultConnection('default')
            ->build();

        $httpResult = $client->run(
            'MATCH (topic:Topic {name: $name}) RETURN topic', //The query is a required parameter
            ['name' => $searchTerm],  //Parameters can be optionally added
            'backup' //The default connection can be overridden
        );
        var_dump($httpResult);
        echo "<hr>";
        $topicArray = $httpResult->first()->get('topic');
        echo $topicArray['name'];
        echo "<hr>";

        $boltResult = $client->run('MATCH (topic:Topic {topicId: 1}) RETURN topic');
        var_dump($boltResult);
        echo "<hr>";

        $statement = new Statement('MATCH (topic:Topic {topicId: $id}) RETURN topic', ['id' => 1]);
        $statementResult = $client->runStatement($statement, 'default');
        var_dump($statementResult);
        echo "<hr>";

        foreach ($client->run('UNWIND range(1, 9) as x RETURN x') as $item) {
            echo $item->get('x');
        }
        var_dump($item);
    }
}
