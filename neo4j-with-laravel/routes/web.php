<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\laudisController;
use App\Http\Controllers\everymanController;
use App\Http\Controllers\httpController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/laudis', [laudisController::class, 'test']);
Route::get('/everyman', [everymanController::class, 'test']);
Route::get('/http', [httpController::class, 'test']);