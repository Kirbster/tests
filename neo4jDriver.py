#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun 21 11:53:53 2021

@author: rachelkirby
"""

# from neo4j import GraphDatabase

# # neo4j driver construction:
# # can use neo4j+ssc to add encryption that accepts self signed certifications
# # can use neo4j+c to add encryption that only accepts certified certificate providers
# # can also use bolt instead of neo4j

# driver = GraphDatabase.driver("neo4j://localhost:7687", auth=("neo4j", "password"))

# def get_topic(tx):
#     result = tx.run("MATCH (n:FieldOfStudy {name: 'Virtual LAN'}) "
#                     "RETURN n.name") 
#     record = result.single()
#     print(record)
    
# with driver.session() as session:
#     session.write_transaction(get_topic)
      
# driver.close()

from neo4j import GraphDatabase
import logging
from neo4j.exceptions import ServiceUnavailable

class App:

    def __init__(self, uri, user, password):
        self.driver = GraphDatabase.driver(uri, auth=(user, password))

    def close(self):
        # Don't forget to close the driver connection when you are finished with it
        self.driver.close()

    def find_topic(self):
        with self.driver.session() as session:
            result = session.read_transaction(self._retrieve_topic_from_db)
            for row in result:
                print("Found topic: {row}".format(row=row))

    @staticmethod
    def _retrieve_topic_from_db(tx):
        query = (
            "MATCH (n:FieldOfStudy {name: 'Virtual LAN'}) "
            "RETURN n.name as name"
        ) 
        result = tx.run(query)
        try:
            return [row["name"] for row in result]
        # Capture any errors along with the query and data for traceability
        except ServiceUnavailable as exception:
            logging.error("{query} raised an error: \n {exception}".format(
                query=query, exception=exception))
            raise

if __name__ == "__main__":
    # Aura queries use an encrypted connection using the "neo4j+s" URI scheme
    uri = "neo4j+ssc://2cc9169a.databases.neo4j.io:7687"
    user = "neo4j"
    password = "HqV3spa9F7E1enMmnJ0cRuvkrbqRXPkQC9tEX1oyXjs"
    app = App(uri, user, password)
    app.find_topic()
    app.close()